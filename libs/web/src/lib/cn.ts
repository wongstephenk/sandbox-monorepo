import { twMerge } from 'tailwind-merge';
import type { ClassValue } from 'clsx';
import { clsx } from 'clsx';

/**
 * A utility function for merging Tailwind classes. This enhances the clsx utility by managing tailwind conflicts.
 */
const cn = (...args: ClassValue[]): string => twMerge(clsx(...args));

export default cn