# web

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test web` to execute the unit tests via [Jest](https://jestjs.io).

https://medium.com/showpad-engineering/how-to-organize-and-name-applications-and-libraries-in-an-nx-monorepo-for-immediate-team-wide-9876510dbe28
